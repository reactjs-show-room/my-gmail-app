export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  DateTime: any;
};

export type MailMessage = {
  __typename?: 'MailMessage';
  _id: Scalars['ID'];
  body: Scalars['String'];
  created?: Maybe<Scalars['DateTime']>;
  subject: Scalars['String'];
  updated?: Maybe<Scalars['DateTime']>;
};

export type MailMessageInput = {
  body: Scalars['String'];
  subject: Scalars['String'];
};

export type MailMessageResolver_MailMessages_SortingInputType = {
  _id?: InputMaybe<SortType>;
  _name_?: Scalars['String'];
  body?: InputMaybe<SortType>;
  created?: InputMaybe<SortType>;
  subject?: InputMaybe<SortType>;
  updated?: InputMaybe<SortType>;
};

export type Mutation = {
  __typename?: 'Mutation';
  createMailMessage: MailMessage;
};


export type MutationCreateMailMessageArgs = {
  input: MailMessageInput;
};

export type Query = {
  __typename?: 'Query';
  mailMessages: Array<MailMessage>;
};


export type QueryMailMessagesArgs = {
  order_by?: InputMaybe<Array<MailMessageResolver_MailMessages_SortingInputType>>;
};

export enum SortType {
  Asc = 'ASC',
  AscNullsFirst = 'ASC_NULLS_FIRST',
  AscNullsLast = 'ASC_NULLS_LAST',
  Desc = 'DESC',
  DescNullsFirst = 'DESC_NULLS_FIRST',
  DescNullsLast = 'DESC_NULLS_LAST'
}

export type Subscription = {
  __typename?: 'Subscription';
  mailMessageAdded: MailMessage;
};
