import * as Types from '../types';

export type AllMailMessagesQueryVariables = Types.Exact<{
  order_by?: Types.InputMaybe<Array<Types.MailMessageResolver_MailMessages_SortingInputType> | Types.MailMessageResolver_MailMessages_SortingInputType>;
}>;


export type AllMailMessagesQuery = { __typename?: 'Query', mailMessages: Array<{ __typename?: 'MailMessage', _id: string, body: string, subject: string }> };

export type OnMailMessageAddedSubscriptionVariables = Types.Exact<{ [key: string]: never; }>;


export type OnMailMessageAddedSubscription = { __typename?: 'Subscription', mailMessageAdded: { __typename?: 'MailMessage', _id: string, body: string, subject: string } };

export type CreateMailMessageMutationVariables = Types.Exact<{
  input: Types.MailMessageInput;
}>;


export type CreateMailMessageMutation = { __typename?: 'Mutation', createMailMessage: { __typename?: 'MailMessage', _id: string, body: string, subject: string, created?: any | null } };
