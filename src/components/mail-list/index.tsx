import * as React from 'react';
import { DataGrid, GridColDef, GridValueGetterParams } from '@mui/x-data-grid';
import { useQuery, useSubscription } from '@apollo/client';
import { allMailMessagesQuery, allMailMessagesSubscription } from '../../graphql/queries';
import { AllMailMessagesQuery, AllMailMessagesQueryVariables, OnMailMessageAddedSubscription, OnMailMessageAddedSubscriptionVariables } from '../../graphql/queries.generated';
import { SortType } from '../../types';

const columns: GridColDef[] = [
  // { field: 'id', headerName: '', width: 150, sortable: false, },
  { field: 'subject', headerName: '', width: 400, sortable: true, },
  { field: 'body', headerName: '', sortable: true, flex: 1 },
  // { field: 'lastName', headerName: 'Last name', width: 130 },
  // {
  //   field: 'age',
  //   headerName: 'Age',
  //   type: 'number',
  //   width: 90,
  // },
  // {
  //   field: 'fullName',
  //   headerName: 'Full name',
  //   description: 'This column has a value getter and is not sortable.',
  //   sortable: false,
  //   width: 160,
  //   valueGetter: (params: GridValueGetterParams) =>
  //     `${params.row.firstName || ''} ${params.row.lastName || ''}`,
  // },
];

// const rows = [
//   { id: 1, lastName: 'Snow', firstName: 'Jon', age: 35 },
//   { id: 2, lastName: 'Lannister', firstName: 'Cersei', age: 42 },
//   { id: 3, lastName: 'Lannister', firstName: 'Jaime', age: 45 },
//   { id: 4, lastName: 'Stark', firstName: 'Arya', age: 16 },
//   { id: 5, lastName: 'Targaryen', firstName: 'Daenerys', age: null },
//   { id: 6, lastName: 'Melisandre', firstName: null, age: 150 },
//   { id: 7, lastName: 'Clifford', firstName: 'Ferrara', age: 44 },
//   { id: 8, lastName: 'Frances', firstName: 'Rossini', age: 36 },
//   { id: 9, lastName: 'Roxie', firstName: 'Harvey', age: 65 },
// ];

export default function MailsTable() {
  const [rows, setRows] = React.useState<any[]>([]);
  const { data: mailMessageData } = useQuery<AllMailMessagesQuery, AllMailMessagesQueryVariables>(allMailMessagesQuery, {
    variables: { order_by: {
      created: SortType.Desc
    } },
    onCompleted: (res) => {
      setRows(res.mailMessages.map(d => ({
        subject: d.subject,
        body: d.body,
        id: d._id
      })))
    }
  });
  
  const { data, loading } = useSubscription<OnMailMessageAddedSubscription, OnMailMessageAddedSubscriptionVariables>(
    allMailMessagesSubscription,
    { variables: { }, onSubscriptionData: (res) => {
      setRows([
        {
          subject: res.subscriptionData.data?.mailMessageAdded.subject,
          body: res.subscriptionData.data?.mailMessageAdded.body,
          id: res.subscriptionData.data?.mailMessageAdded._id
        },
        ...rows,
      ])
    } }
  );
  return (
    <div style={{ height: 'calc(100vh - 200px)', width: '100%' }}>
      <DataGrid
        rows={rows}
        columns={columns}
        pageSize={50}
        rowsPerPageOptions={[5]}
        checkboxSelection
      />
    </div>
  );
}