import React from 'react';
import logo from './logo.svg';
import './App.css';
import { useQuery, useSubscription } from '@apollo/client';
import { allMailMessagesQuery, allMailMessagesSubscription } from './graphql/queries';
import { MailMessage } from './types';
import { AllMailMessagesQuery, AllMailMessagesQueryVariables, OnMailMessageAddedSubscription, OnMailMessageAddedSubscriptionVariables } from './graphql/queries.generated';

function Main() {
  const { data: mailMessageData } = useQuery<AllMailMessagesQuery, AllMailMessagesQueryVariables>(allMailMessagesQuery, {
    variables: { },
  });
  
  
  const { data, loading } = useSubscription<OnMailMessageAddedSubscription, OnMailMessageAddedSubscriptionVariables>(
    allMailMessagesSubscription,
    { variables: { }, onSubscriptionData: (res) => {
      console.log(res.subscriptionData.data?.mailMessageAdded)
    } }
  );
  
  return (
      <div>
        {data && (
            <div>
              {data.mailMessageAdded._id}
            </div>
          )}
          <div>
            {mailMessageData && (
              mailMessageData.mailMessages.map(m => (
                <div key={m._id}>
                  {m.subject} <br />
                  {m.body}
                </div>
              ))
            )}
          </div>
      </div>
  );
}

export default Main;
