import { gql } from "@apollo/client";

export const allMailMessagesQuery = gql`
    query allMailMessages (
        $order_by: [MailMessageResolver_MailMessages_SortingInputType!] = [
          { _name_: "SortingPropertyDecorator" }
        ]
      ) {
        mailMessages (order_by:$order_by) {
            _id
            body
            subject
          }
    }
`;

export const allMailMessagesSubscription = gql`
  subscription OnMailMessageAdded {
    mailMessageAdded {
        _id
        body
        subject
    }
  }
`;

export const createMailMessageSubscription = gql`
    mutation createMailMessage($input:MailMessageInput!) {
        createMailMessage(input:$input){
            _id
            body
            subject
            created
      }
  }
`;